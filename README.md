# Debug tools Docker image

This repository contains a Dockerfile with various tools and CLI's installed.

## Table of Contents

- [Usage](#usage)
- [Contribute](#contribute)
- [License](#license)

## Usage

```bash
docker run -it --net host registry.gitlab.com/avisi/debug-tools
```

## Contribute

PRs accepted. All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/debug-tools/issues).

## License

[MIT © Avisi B.V.](LICENSE)
